<?php

/**
 * Expose field values on entities as conditions
 */
class context_fields_condition_field extends context_condition {

  function __construct($plugin, $info) {
    parent::__construct($plugin, $info);
    $this->field_name = isset($info['context_fields_field']) ? $info['context_fields_field'] : NULL;
    $this->field = !empty($this->field_name) ? field_info_field($this->field_name) : NULL;
    $this->field_column = isset($info['context_fields_field_column']) ? $info['context_fields_field_column'] : NULL;
  }

  /**
   * Condition form.
   */
  function condition_form($context) {
    $default = $this->fetch_from_context($context, 'values');
    return array(
      '#title' => $this->title,
      '#description' => $this->description . t(' Leave blank to set the context on all entities that have this field.'),
      '#type' => 'textfield',
      '#default_value' => (!empty($default) && is_array($default)) ? reset($default) : '',
    );
  }

  /**
   * Condition form submit handler.
   */
  function condition_form_submit($values) {
    return array($values);
  }

  function field_column() {
    return isset($this->field_column) ? $this->field_column : 'value';
  }

  /**
   * @param $entity_type
   * @param $entity
   * @param $op
   *  - 'view': We're viewing the entity
   *  - 'form': We're editing/creating the entity
   */
  function execute($entity_type, $entity, $op) {

    $values = $this->getFieldValuesWithEntityMetaData($entity_type, $entity);

    // If Entity meta stuff fails, use a more basic approach to
    // getting the field value.
    if (!isset($values)) {
      $this->getFieldValuesManually($entity_type, $entity);
    }

    // Match all contexts that had this value
    if (!empty($values)) {
      foreach ($values as $val) {
        foreach ($this->get_contexts($val) as $context) {
          $this->condition_met($context, $val);
        }
      }
    }

    // Match all contexts that were blank - and as such match everything
    foreach ($this->get_contexts('') as $context) {
      $this->condition_met($context, '');
    }

  }

  function getFieldValuesWithEntityMetaData($entity_type, $entity) {
    $values = NULL;
    try {
      $wrapper = entity_metadata_wrapper($entity_type, $entity);
      if (!empty($wrapper)) {
        $values = $this->get_field_values($wrapper->{$this->field_name});
      }
    }
    catch (Exception $e) {
      // Entity meta stuff failed for some reason
    }
    return $values;
  }

  function getFieldValuesManually($entity_type, $entity) {
    $values = array();
    $field_column = $this->field_column();
    $items = field_get_items($entity_type, $entity, $this->field_name);
    if (!empty($items)) {
      foreach ($items as $item) {
        if (is_array($item) && isset($item[$field_column])) {
          $values[$item[$field_column]] = $item[$field_column];
        }
      }
    }
    return $values;
  }

  function get_field_values($wrapper) {
    $values = array();
    if ($wrapper instanceof EntityListWrapper) {
      foreach ($wrapper as $value) {
        $values[] = $this->get_field_values($value);
      }
    }
    elseif ($wrapper instanceof EntityDrupalWrapper) {
      $values[] = $wrapper->getIdentifier();
    }
    elseif ($wrapper instanceof EntityValueWrapper) {
      $values[] = $wrapper->value();
    }
    elseif ($wrapper instanceof EntityMetadataWrapper) {
      $values[] = $wrapper->value->raw();
    }
    return $values;
  }
}
