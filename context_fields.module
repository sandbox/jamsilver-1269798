<?php


/**
 * Implements hook_context_node_condition_alter().
 */
function context_fields_context_node_condition_alter(&$node, &$op) {
  context_fields_entity_condition('node', $node, $op);
}

/**
 * Centralized entity condition call function for all the ways
 * to get at an entity view / entity edit form.
 */
function context_fields_entity_condition($entity_type, $entity, $op) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  if (!empty($bundle)) {
    $fields = field_info_instances($entity_type, $bundle);
    foreach ($fields as $field) {
      if ($plugin = context_get_plugin('condition', 'context_fields_' . $field['field_name'])) {
        $plugin->execute($entity_type, $entity, $op);
      }
    }
  }
}

/**
 * Implementation of hook_context_registry().
 */
function context_fields_context_registry() {
  $registry = array();

  $fields = field_info_fields();
  $field_types = field_info_field_types();
  $entity_info = entity_get_info();

  foreach ($fields as $field_name => $info) {

    list($most_common_label, $all_labels) = _context_fields_get_field_labels($field_name);

    $registry['conditions']['context_fields_' . $field_name] = array(
      'title' => t('Field: @field_name', array('@field_label' => $most_common_label, '@field_name' => $field_name)),
      'description' => t('Set this context when viewing an entity which has the field %common_label (@field_name) with a certain value.', array('%common_label' => $most_common_label, '@field_name' => $field_name)),
      'plugin' => 'context_fields_condition_field',
      'context_fields_field' => $field_name,
      'context_fields_field_column' => !empty($info['columns']) ? key($info['columns']) : 'value',
    );

    if (!empty($all_labels) && is_array($all_labels)) {
      $registry['conditions']['context_fields_' . $field_name]['description'] .= t(' This field is also known as: %list_of_labels.', array('%list_of_labels' => implode(',', $all_labels)));
    }

    if (strpos($info['type'], 'reference') !== FALSE) {
      $registry['conditions']['context_fields_' . $field_name]['plugin'] = 'context_fields_condition_field_reference';
    }
  }

  return $registry;
}

/**
 * Implementation of hook_context_plugins().
 */
function context_fields_context_plugins() {
  $plugins = array();

  // Handles all 'basic' fields
  $plugins['context_fields_condition_field'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'context_fields'),
      'file' => 'context_fields_condition_field.inc',
      'class' => 'context_fields_condition_field',
      'parent' => 'context_condition',
    ),
  );

  // Handles all 'basic' fields
  $plugins['context_fields_condition_field_reference'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'context_fields'),
      'file' => 'context_fields_condition_field_reference.inc',
      'class' => 'context_fields_condition_field_reference',
      'parent' => 'context_fields_condition_field',
    ),
  );

  return $plugins;
}

/**
 * Helper function that returns an array of labels
 * by which this field is known and also the most commonly
 * used label.
 * Totally stolen from views module.
 * @see field_views_field_label()
 */
function _context_fields_get_field_labels($field_name) {
  $label_counter = array();
  $all_labels = array();
  // Count the amount of instances per label per field.
  $instances = field_info_instances();
  foreach ($instances as $entity_name => $entity_type) {
    foreach ($entity_type as $bundle) {
      if (isset($bundle[$field_name])) {
        $label_counter[$bundle[$field_name]['label']] = isset($label_counter[$bundle[$field_name]['label']]) ? ++$label_counter[$bundle[$field_name]['label']] : 1;
        $all_labels[$bundle[$field_name]['label']] = TRUE;
      }
    }
  }
  if (empty($label_counter)) {
    return array($field_name, $all_labels);
  }
  // Sort the field lables by it most used label and return the most used one.
  arsort($label_counter);
  $label_counter = array_keys($label_counter);
  unset($all_labels[$label_counter[0]]);
  $all_labels = array_keys($all_labels);
  return array($label_counter[0], $all_labels);
}