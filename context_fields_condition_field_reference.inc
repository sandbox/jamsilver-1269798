<?php

/**
 * Expose field values on entities as conditions
 */
class context_fields_condition_field_reference extends context_fields_condition_field {


  /**
   * Condition form.
   */
  function condition_form($context) {
    $default = $this->fetch_from_context($context, 'values');

    $form = array();

    $form = array(
      '#title' => $this->title,
      '#description' => $this->description . t(' Leave blank to set the context on all entities that have this field.'),
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
    );

    $form['value'] = array(
      '#type' => 'textfield',
      '#default_value' => !empty($default['value']) ? $default['value'] : '',
      '#states' => array(
        'visible' => array(
          '#edit-conditions-plugins-' . str_replace('_', '-', $this->plugin) . '-values-use-referenced-content-type' => array('checked' => FALSE),
        )
      ),
    );

    $form['use_referenced_content_type'] = array(
      '#title' => t('Restrict based on bundle of referenced entity'),
      '#type' => 'checkbox',
      '#default_value' => !empty($default['referenced_content_type']),
    );

    $bundles = array();
    foreach (entity_get_info() as $entity_type => $entity_info) {
      foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
        //if (isset($this->field['bundles'][$entity_type]) && in_array($bundle_name, $this->field['bundles'][$entity_type])) {
          $bundles['!contenttype!' . $entity_type . '::' . $bundle_name] = t('@entity_type_label: @bundle_label', array('@entity_type_label' => $entity_info['label'], '@bundle_label' => $bundle_info['label']));
        //}
      }
    }

    $form['referenced_content_type'] = array(
      '#title' => t('Referenced entity is of Entity:Bundle'),
      '#type' => 'checkboxes',
      '#options' => $bundles,
      '#default_value' => isset($default['referenced_content_type']) ? $default : array(),
      '#states' => array(
        'visible' => array(
          '#edit-conditions-plugins-' . str_replace('_', '-', $this->plugin) . '-values-use-referenced-content-type' => array('checked' => TRUE),
        )
      ),
    );

    return $form;
  }

  /**
   * Condition form submit handler.
   */
  function condition_form_submit($values) {

    // Checkboxes treatment
    if (isset($values['referenced_content_type'])) {
      $values['referenced_content_type'] = array_filter($values['referenced_content_type']);
    }

    if (!empty($values['use_referenced_content_type']) && !empty($values['referenced_content_type'])) {
      $firstkey = key($values['referenced_content_type']);
      $firstval = $values['referenced_content_type'][$firstkey];
      unset($values['referenced_content_type'][$firstkey]);
      $values = $values['referenced_content_type'];
      $values['referenced_content_type'] = $firstval;
    }
    else {
      unset($values['referenced_content_type']);
    }

    unset($values['use_referenced_content_type']);

    return $values;
  }

  /**
   * @param $entity_type
   * @param $entity
   * @param $op
   *  - 'view': We're viewing the entity
   *  - 'form': We're editing/creating the entity
   */
  function execute($entity_type, $entity, $op) {

    $values = $this->getFieldValuesWithEntityMetaData($entity_type, $entity);

    // Match all contexts that had this value
    if (!empty($values)) {
      foreach ($values as $val) {
        // In addition allow contexts to match if they have the content type of referenced entities.
        if (is_array($val)) {
          $value = '!contenttype!' . $val['type'] . '::' . $val['bundle'];
          foreach ($this->get_contexts($value) as $context) {
            $this->condition_met($context, $value);
          }
          // Allow the original test against entity_id also
          $val = $val['id'];
        }
        foreach ($this->get_contexts($val) as $context) {
          $this->condition_met($context, $val);
        }
      }
    }

    // Match all contexts that were blank - and as such match everything
    foreach ($this->get_contexts('') as $context) {
      $this->condition_met($context, '');
    }

  }

  function get_field_values($wrapper) {
    $values = array();
    if ($wrapper instanceof EntityListWrapper) {
      foreach ($wrapper as $value) {
        $values += $this->get_field_values($value);
      }
    }
    elseif ($wrapper instanceof EntityDrupalWrapper) {
      // Instead of just returning entity id, return lots of info about the referenced entity
      list($id, $vid, $bundle) = entity_extract_ids($wrapper->type(), $wrapper->value());
      $values[] = array(
        'entity' => $wrapper->value(),
        'type' => $wrapper->type(),
        'id' => $id,
        'bundle' => $bundle,
      );
    }
    elseif ($wrapper instanceof EntityValueWrapper) {
      $values[] = $wrapper->value();
    }
    elseif ($wrapper instanceof EntityMetadataWrapper) {
      $values[] = $wrapper->value->raw();
    }
    return $values;
  }
}
